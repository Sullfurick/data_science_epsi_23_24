import os
import pandas as pd

########################################################################################################################
# VARIABLES
########################################################################################################################
BASE_DATA_FILE_PATH = '../'  # edit this value to find csv file in the project


########################################################################################################################
# CSV FILE & DATASET
########################################################################################################################
def create_sub_dataset(_file_in, _file_out='gladiator_data_500.csv', _len=500):
    """
    Development purpose
    """
    _content = pd.read_csv(_file_in)
    _out = os.path.join(os.path.dirname(os.path.abspath(__file__)), BASE_DATA_FILE_PATH, _file_out)
    _content[0: _len].to_csv(os.path.join(BASE_DATA_FILE_PATH, _out))


def read_data(_csv_file):
    """
    Read a csv file
    Return the content as pandas dataframe
    """
    _data = pd.read_csv(_csv_file)
    return _data


def locate_csv_file(_file_name):
    _file = os.path.join(os.path.dirname(os.path.abspath(__file__)), BASE_DATA_FILE_PATH, _file_name)

    if not os.path.exists(_file):
        raise FileNotFoundError(f"The file at {_file} does not exist.")

    if not _file.lower().endswith('.csv'):  # todo mimetype
        raise ValueError(f"The file at {_file} is not a CSV file.")

    return _file


########################################################################################################################
# DATA METHODS
########################################################################################################################
def print_primary_stats(_dataframe):
    """
    Base pandas library analysis
    """
    print('***** PRIMARY STATS *****')
    print(_dataframe.head())
    print(_dataframe.shape)
    print(_dataframe.columns)
    print(_dataframe.dtypes)


def print_advanced_stats(_dataframe):
    """
    Custom analysis
    """
    print('***** ADVANCED STATS *****')
    unic_col_types = set(_dataframe.dtypes)

    # Number of column by type
    col_type_nb = {
        f'{_type}': nb_col_for_type(_dataframe, _type)
        for _type in unic_col_types
    }
    print_stats_dict(_d=col_type_nb, _title='Nb col by type')

    # Column number by type
    cols_by_type = {
        f'{t}': [c for c in all_col_of_one_type(_dataframe, t)]
        for t, _ in col_type_nb.items()
    }
    print_stats_dict(_d=cols_by_type, _title='Cols by type')

    # Unique values by column (object type only, 20 uniques values max)
    # Helps us to find features to use One Hot Encode on
    unique_values = {
        f'{c}': f'{_dataframe[c].unique() if len(_dataframe[c].unique()) < 20 else len(_dataframe[c].unique())}'
        for c in cols_by_type.get('object')
    }
    print_stats_dict(_d=unique_values, _title='Unique values for object type column (limit 20)')


def nb_col_for_type(_dataframe, _type):
    """
    Return the number of columns for one given type 
    """
    s = 0
    for col in _dataframe.columns:
        s += (_type == _dataframe[col].dtype)
    return s


def all_col_of_one_type(_dataframe, _type):
    """
    Return all columns of a given type as a sub dataframe
    """
    return _dataframe.select_dtypes(include=_type)


def print_stats_dict(_d, _title):
    """
    Print a analysis dict with title (huho)
    """
    print(f'***** {_title} *****')
    for k, v in _d.items():
        print(f'{k} : {v}')


########################################################################################################################
# EXECUTION
########################################################################################################################
def process(_csv_file):
    try:
        data = locate_csv_file(_csv_file)
    except (FileNotFoundError, ValueError) as file_err:
        print(file_err)
    else:
        d = read_data(data)
        print_primary_stats(d)
        print_advanced_stats(d)


if __name__ == '__main__':
    process('gladiator_data.csv')
    # create_sub_dataset(_file_in='gladiator_data.csv', _file_out='huho2.csv')
