# Évaluation de module - Data Science & Machine Learning

### I1 EISI - **EPSI** RENNES - 2023-2024
Formateur : **Alain CARIOU**

Groupe : 
- **Olivier BRICAUD**
- **Aurélien GOURIOU**
- **Stephen PROUST**

____________________________________________________

### SUJET
Le but ici est de choisir un **dataset** intéressant, afin d'utiliser les données qu'il contient
pour entraîner différents **algorithmes d'apprentissages**. L'objectif est **d'effectuer des prédictions** ou de **résoudre
des problèmes** grâce à ces algorithmes.

Les recherches doivent être consignées dans un **rapport** (qui prendra la forme de ce README), qui présentera le **jeu de données** utilisé,
les **algorithmes** utilisées, les résultats obtenus, ainsi que les **difficultés rencontrées**. Il peut être également
intéressant de présenter les forces et faiblesses des algorithmes utilisés, ainsi que les axes d'améliorations dégagés.

____________________________________________________

### DATASET
Le **dataset** choisi est le suivant : https://www.kaggle.com/datasets/anthonytherrien/gladiator-combat-records-and-profiles-dataset

Ce dataset contient des données imaginaires et non historiques.

En voici un échantillon :

| Name               | Age | Birth Year | Origin | Height | Weight | Category    | Wins | Losses | Special Skills | Weapon of Choice | Patron Wealth | Equipment Quality | Public Favor       | Injury History | Mental Resilience  | Diet and Nutrition | Tactical Knowledge | Allegiance Network | Battle Experience | Psychological Profile | Health Status | Personal Motivation | Previous Occupation | Training Intensity | Battle Strategy | Social Standing | Crowd Appeal Techniques | Survived |
|--------------------|-----|------------|--------|--------|--------|-------------|------|--------|----------------|------------------|---------------|-------------------|--------------------|----------------|--------------------|--------------------|--------------------|--------------------|-------------------|-----------------------|---------------|---------------------|---------------------|--------------------|-----------------|-----------------|-------------------------|----------|
| Cosconius Hostius  | 42  | 8          | Gaul   | 176    | 54     | Hoplomachus | 10   | 2      | Novice         | Spear            | Low           | Basic             | 0.7452501581541044 | Low            | 2.30962240621191   | Poor               | Basic              | Strong             | 12                | Stoic                 | Good          | Glory               | Laborer             | Medium             | Balanced        | Low             | Flamboyant              | True     |
| Caecilius Valerius | 30  | 20         | Greece | 173    | 67     | Provocator  | 6    | 0      | Agility        | Dagger           | High          | Superior          | 0.570122220620966  | Low            | 2.6095120427204916 | Excellent          | Expert             | Strong             | 6                 | Aggressive            | Excellent     | Freedom             | Laborer             | Medium             | Aggressive      | High            | Intimidating            | False    |
| Sempronius Aulus   | 30  | 20         | Gaul   | 171    | 81     | Hoplomachus | 8    | 4      | Endurance      | Spear            | Low           | Basic             | 0.6053981535223177 | Low            | 8.770731237200259  | Poor               | Advanced           | Moderate           | 12                | Calculative           | Excellent     | Wealth              | Unemployed          | Medium             | Balanced        | Medium          | Charismatic             | True     |

Ce dataset contient au total 737 570 lignes et 29 colonnes.

### TRAITEMENT DES DONNÉES

#### Dataset Analysis

```sh
python -m venv venv
source venv/bin/activate
python src/data.py
```

```txt
***** ADVANCED STATS *****
***** Nb col by type *****
bool : 1
int64 : 7
float64 : 2
object : 19
***** Cols by type *****
bool : ['Survived']
int64 : ['Age', 'Birth Year', 'Height', 'Weight', 'Wins', 'Losses', 'Battle Experience']
float64 : ['Public Favor', 'Mental Resilience']
object : ['Name', 'Origin', 'Category', 'Special Skills', 'Weapon of Choice', 'Patron Wealth', 'Equipment Quality', 'Injury History', 'Diet and Nutrition', 'Tactical Knowledge', 'Allegiance Network', 'Psychological Profile', 'Health Status', 'Personal Motivation', 'Previous Occupation', 'Training Intensity', 'Battle Strategy', 'Social Standing', 'Crowd Appeal Techniques']
***** Unique values for object type column (limit 20) *****
Name : 32761
Origin : ['Gaul' 'Greece' 'Rome' 'Thrace' 'Numidia' 'Germania']
Category : ['Hoplomachus' 'Provocator' 'Retiarius' 'Secutor' 'Murmillo' 'Thraex']
Special Skills : ['Novice' 'Agility' 'Endurance' 'Tactics' 'Speed' 'Strength']
Weapon of Choice : ['Spear' 'Dagger' 'Net' 'Gladius (Sword)' 'Sica (Curved Sword)' 'Trident']
Patron Wealth : ['Low' 'High' 'Medium']
Equipment Quality : ['Basic' 'Superior' 'Standard']
Injury History : ['Low' 'High']
Diet and Nutrition : ['Poor' 'Excellent' 'Adequate']
Tactical Knowledge : ['Basic' 'Expert' 'Advanced' 'Intermediate']
Allegiance Network : ['Strong' 'Moderate' 'Weak']
Psychological Profile : ['Stoic' 'Aggressive' 'Calculative' 'Fearful']
Health Status : ['Good' 'Excellent' 'Fair']
Personal Motivation : ['Glory' 'Freedom' 'Wealth' 'Survival' 'Vengeance']
Previous Occupation : ['Laborer' 'Unemployed' 'Entertainer' 'Soldier' 'Criminal']
Training Intensity : ['Medium' 'Low' 'High']
Battle Strategy : ['Balanced' 'Aggressive' 'Defensive']
Social Standing : ['Low' 'High' 'Medium']
Crowd Appeal Techniques : ['Flamboyant' 'Intimidating' 'Charismatic' 'Humble']
```
____________________________________________________

### ALGORITHMES

#### 1. KNN

#### 2. SVM

#### 3. ????

#### 4. ????

____________________________________________________

### RÉSULTATS

____________________________________________________

### DIFFICULTÉS RENCONTRÉES

____________________________________________________

### LANGAGES ET LIBRAIRIES UTILISÉS
Langage :
- Python (version 3.10+)

Librairies :
- Pandas
- Numpy
- Matplotlib
- Scikit-learn
- ...

____________________________________________________

